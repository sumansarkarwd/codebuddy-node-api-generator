#!/usr/bin/env node

import chalk from 'chalk';
import inquirer from 'inquirer';
import chalkAnimation from 'chalk-animation';
import sleep from '../utils/sleep.js';
import log from '../utils/log.js';
import config from '../utils/config.js';
import commandRun from '../utils/commandRun.js';
import folderIsEmpty from '../utils/folderIsEmpty.js';

// await welcome();

// await selectTemplate();

async function welcome() {
  const welcomeTitle = chalkAnimation.rainbow(
    'Let\'s build an awesome API!  \n',
  );
  await sleep(1000);
  welcomeTitle.stop();
}

// eslint-disable-next-line no-unused-vars
async function generate(database, orm, ts) {
  const currentWorkingDirectory = process.cwd();

  const folderEmpty = await folderIsEmpty(currentWorkingDirectory);

  if (!folderEmpty) {
    log(chalk.red('Folder is not empty!'));
    process.exit(0);
  }

  const {
    gitRepo,
    folderName,
  } = config.databases.find((db) => db.name === database).orms.find((o) => o.name === orm);

  const cloningGitRepoAnim = chalkAnimation.rainbow('Cloning Git Repo...');
  await commandRun(`git clone --depth 1 --branch master ${gitRepo}`);
  cloningGitRepoAnim.stop();

  const copingFilesAnim = chalkAnimation.rainbow('Copying files...');
  await commandRun(`cp -R ./${folderName}/. .`);
  copingFilesAnim.stop();

  const npmInstall = chalkAnimation.rainbow('Running npm install...');
  await commandRun('npm install --quite');
  npmInstall.stop();

  const cleaningUpAnim = chalkAnimation.rainbow('Cleaning up...');
  await commandRun(`rm -rf ./${folderName}`);
  await commandRun('rm -rf ./.git');
  await commandRun('git init');
  cleaningUpAnim.stop();

  log(chalk.green('All set, run `cp .env.example .env` and start coding!'));
  log(chalk.green('Happy hacking!!!'));
}

async function selectTemplate() {
  log(chalk.green('\nSelect a template to start building your API!\n'));

  const { database } = await inquirer.prompt([
    {
      type: 'list',
      name: 'database',
      message: 'Select Database:',
      choices: config.databases.map((d) => d.name),
    },
  ]);

  const { orms } = config.databases.find((db) => db.name === database);

  const { orm } = await inquirer.prompt([
    {
      type: 'list',
      name: 'orm',
      message: 'Select ORM:',
      choices: orms.map((o) => o.name),
    },
  ]);

  const { implemented } = orms.find((o) => o.name === orm);

  if (!implemented) {
    log(chalk.red(`${orm} is not implemented yet!`));
    process.exit(0);
  }

  const { ts } = await inquirer.prompt([
    {
      type: 'list',
      name: 'ts',
      message: 'Do you want TS:',
      choices: ['Yes', 'No'],
    },
  ]);

  if (ts === 'Yes') {
    log(chalk.red('TS Support is not implemented yet!'));
    process.exit(0);
  }

  await sleep(1000);

  await generate(database, orm, ts);
}

await welcome();
await selectTemplate();
