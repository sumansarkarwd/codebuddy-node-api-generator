# A CLI tool to generate boilerplate code for NODE API development.

## Instructions

Install the CLI tool globally by running...

```bash
$ npm install -g codebuddy-node-api-generator
```

Now, you can use the CLI tool to generate boilerplate code for your NODE API by running...

```bash
$ node-api-generate
```

## Roadmap

- [x] Mysql + Sequelize ORM + Vanilla JS
- [ ] Postgres SQL + Type ORM + Vanilla JS
- [x] MongoDb + Mongoose + Vanilla JS
- [ ] Postgres SQL + Type ORM + TypeScript
- [ ] MongoDb + Mongoose + TypeScript
- [ ] Docker + Docker Compose
- [ ] Gitlab CI