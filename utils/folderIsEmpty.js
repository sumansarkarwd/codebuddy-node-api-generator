import fs from 'fs';

export default (folderPath) => new Promise((resolve, reject) => {
  try {
    fs.readdir(folderPath, (err, files) => {
      if (err) {
        if (err.code === 'ENOENT') {
          resolve(true);
        } else {
          reject(err);
        }
      } else {
        resolve(files.length === 0);
      }
    });
  } catch (err) {
    reject(err);
  }
});
