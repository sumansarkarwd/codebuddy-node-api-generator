import { exec } from 'child_process';
import chalk from 'chalk';
import log from './log.js';

export default (command = '') => new Promise((resolve, reject) => {
  try {
    exec(command, (error, stdout) => {
      if (error) {
        log(chalk.red(error));
        reject(error);
      } else if (stdout) {
        log(chalk.blue(stdout));
      }
      resolve(true);
    });
  } catch (err) {
    log(chalk.red(err));
    reject();
  }
});
