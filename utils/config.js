export default {
  databases: [
    {
      name: 'MongoDB',
      orms: [
        {
          name: 'Mongoose',
          implemented: true,
          gitRepo: 'https://gitlab.com/sumansarkarwd/node-mongodb-mongoose.git',
          folderName: 'node-mongodb-mongoose',
        },
      ],
    },
    {
      name: 'MySQL',
      orms: [
        {
          name: 'Sequelize',
          implemented: true,
          gitRepo: 'https://gitlab.com/sumansarkarwd/node-mysql-sequelize.git',
          folderName: 'node-mysql-sequelize',
        },
        {
          name: 'TypeORM',
          implemented: false,
        },
      ],
    },
    {
      name: 'Postgres',
      orms: [
        {
          name: 'Sequelize',
          implemented: true,
        },
        {
          name: 'TypeORM',
          implemented: false,
        },
      ],
    },
  ],
};
